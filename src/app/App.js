/**
 * This program is a state-full React
 * Component managing the state for the
 * Data Table and handles table data entry
 * for the input form.
 *
 * @author  Smit Patel
 * @version 1.0.0
 * @since   28 Sep, 2018
 */

import React, { Component } from 'react';
import DataTable from './data-table/DataTable';
import Form from './form/Form';
import './App.css';
// Load the api end point
import { DATA_END_POINT_URL } from '../assets/js/constants';

class App extends Component {
  // Handles row entry for the Data Table inside the Form component
  addRowToDataTable = (event, name, last_name, _class) => {
    let id = this.state.id + 1;
    let row = {
      id,
      name,
      last_name,
      class: _class
    };
    this.setState({
      id,
      remoteDataTable: [...this.state.remoteDataTable, row]
    });
  };

  constructor(props) {
    super(props);
    this.state = {
      id: 0, // Acts as a unique key for each subsequent row in the table
      remoteDataTable: [] // Maintains the state of the table data
    };
  }

  async componentDidMount() {
    let newState = await this.fetchTableData();
    if (newState !== null) this.setState(newState);
  }

  /* Gets the table data from the end-point
   * CARE: ASYNC NETWORK I/O OPERATIONS
   */
  fetchTableData = async () => {
    try {
      let response = await fetch(DATA_END_POINT_URL);
      let data = await response.json();
      let { id } = this.state;
      let rows = data.data.map(elem => {
        id = id + 1;
        return { id, ...elem };
      });
      return { id, remoteDataTable: rows };
    } catch (err) {
      console.error('Something went wrong while fetching data');
      return null;
    }
  };

  render() {
    // Pass the table rows to the table presentational layer
    const { remoteDataTable } = this.state;
    return (
      <div className="app-container">
        <div className="sidebar-left" />
        <div className="table-container">
          <DataTable rows={remoteDataTable} />
          <Form addTableEntry={this.addRowToDataTable} />
        </div>
        <div className="sidebar-right" />
      </div>
    );
  }
}

export default App;
