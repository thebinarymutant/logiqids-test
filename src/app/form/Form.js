import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Messenger from '../shared/messenger/Messenger';
import './Form.css';

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      class: undefined,
      last_name: '',
      name: '',
      open: false,
      openMessenger: false
    };
    // Store different alert message here
    this.message = {
      error: 'Please fill in the details properly',
      success: 'Row updated successfully!'
    };
  }

  // Form-modal is opened here
  handleClickOpen = () => {
    this.setState({ open: true });
  };

  // Form-modal is reset and closed here
  handleClose = event => {
    this.setState({
      open: false,
      class: undefined,
      last_name: '',
      name: ''
    });
  };

  // Form-modal input handler
  handleInputChanges = event => {
    let { value } = event.target;
    this.setState({ [event.target.id]: value });
  };

  // Alert box terminator
  handleMessengerClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    this.setState({ openMessenger: false });
  };

  render() {
    const { openMessenger, name, last_name } = this.state;
    return (
      <div className="form-entry">
        <Button
          variant="contained"
          color="primary"
          onClick={this.handleClickOpen}
        >
          Add More
        </Button>
        <Messenger
          open={openMessenger}
          handleClose={this.handleMessengerClose}
          variant={this.messengerVariant}
          message={this.message[this.messengerVariant]}
        />
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Enter your Details</DialogTitle>
          <DialogContent>
            <DialogContentText>
              All the fields are required to be filled.
            </DialogContentText>
            <TextField
              autoFocus
              required
              margin="dense"
              id="name"
              label="Name"
              type="text"
              variant="outlined"
              onChange={this.handleInputChanges}
              value={name}
              fullWidth
            />
            <TextField
              required
              margin="dense"
              id="last_name"
              label="Last Name"
              type="text"
              variant="outlined"
              onChange={this.handleInputChanges}
              value={last_name}
              fullWidth
            />
            <TextField
              required
              margin="dense"
              id="class"
              label="Class"
              type="number"
              variant="outlined"
              onChange={this.handleInputChanges}
              fullWidth
            />
          </DialogContent>
          <DialogActions className="form-entry">
            <Button onClick={this.saveFormData} color="primary">
              Save
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }

  //
  saveFormData = event => {
    let { name, last_name } = this.state;
    // Regex to match ontegers
    let pattern = /^[0-9]{1,3}$/;
    let _class = this.state.class;
    // Tests for integer
    let test = pattern.test(_class);
    // Update table row if all test pass else respond with error
    if (name.length > 0 && last_name.length > 0 && test) {
      this.props.addTableEntry(event, name, last_name, _class);
      this.messengerVariant = 'success';
      this.handleClose();
    } else {
      this.messengerVariant = 'error';
    }

    // Display the alert according to pervious test of success or failure
    this.setState({
      openMessenger: true
    });

  };
}

export default Form;
