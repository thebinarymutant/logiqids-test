/**
 * This program is a state-less React
 * Component which acts as a Tabular
 * Representation for the word frequency
 * data requesed by the user.
 *
 * @author  Smit Patel
 * @version 1.0.0
 * @since   28 Sep, 2018
 */

import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import './DataTable.css';

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: 'lightgrey',
    color: 'black',
    fontSize: 16,
    border: '2px solid black',
    textAlign: 'center'
  },
  body: {
    fontSize: 14,
    border: '2px solid black',
    textAlign: 'center'
  }
}))(TableCell);

// Styles for the creating CustomDataTable
const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto'
  },
  table: {
    minWidth: 0,
    textTransform: 'capitalize'
  }
});

const DataTable = props => {
  const { classes, rows } = props;

  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <CustomTableCell>Name</CustomTableCell>
            <CustomTableCell>Last Name</CustomTableCell>
            <CustomTableCell>Class</CustomTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map(row => {
            return (
              <TableRow key={row.id}>
                <CustomTableCell component="th" scope="row">
                  {row.name}
                </CustomTableCell>
                <CustomTableCell>{row.last_name}</CustomTableCell>
                <CustomTableCell>{row.class}</CustomTableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Paper>
  );
};

export default withStyles(styles)(DataTable);
